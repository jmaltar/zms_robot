import math

from helpers.body import State, Body

from robots.sensor import Sensor

import numpy as np

class Specs:
    __slots__ = 'wheel_radius', 'base_length', 'cpr', 'v_max', 'min_rpm', 'max_rpm', 'angular_wheel_min', \
                'angular_wheel_max'

    def __init__(self, wheel_radius, base_length, cpr, v_max, min_rpm, max_rpm):
        self.wheel_radius = wheel_radius
        self.base_length = base_length
        self.cpr = cpr
        self.v_max = v_max
        self.min_rpm = min_rpm
        self.max_rpm = max_rpm
        self.angular_wheel_min = self.min_rpm * 2 * math.pi / 60
        self.angular_wheel_max = self.max_rpm * 2 * math.pi / 60


class Wheel:
    __slots__ = 'nr_of_ticks', 'nr_of_revolutions', 'cpr', 'wheel_radius'

    def __init__(self, wheel_radius, cpr):
        self.nr_of_ticks = 0
        self.nr_of_revolutions = 0
        self.wheel_radius = wheel_radius
        self.cpr = cpr

    def update_ticks(self, v, dt):
        self.nr_of_revolutions += v*dt/2/math.pi
        self.nr_of_ticks = int(self.nr_of_revolutions*self.cpr)


class Mathbot(Body):
    __slots__ = 'specs', 'left_wheel', 'right_wheel', 'chassis_body', 'chassis', 'envelope', 'wheel_points',\
                'sensors', 'left_wheel_body', 'right_wheel_body'

    def __init__(self, state, specs):

        side_length = 0.062
        height = 0.161
        xi = 45 * 2 * math.pi / 360
        wheel_width = 0.026
        wheel_base_full = 0.1
        aluminium_height = 0.024
        wheel_radius = 0.034
        side_length_env = 0.088
        height_env = 0.192

        self.chassis = [
            (-aluminium_height / 2, -(wheel_base_full - wheel_width)),
            (aluminium_height / 2, -(wheel_base_full - wheel_width)),
            (height - 2 * math.sin(xi) * side_length - side_length, -side_length / 2),
            (height - math.sin(xi) * side_length - side_length, -side_length / 2 - math.cos(xi) * side_length),
            (height - math.sin(xi) * side_length, -side_length / 2 - math.cos(xi) * side_length),
            (height, -side_length / 2),
            (height, side_length / 2),
            (height - math.sin(xi) * side_length, side_length / 2 + math.cos(xi) * side_length),
            (height - math.sin(xi) * side_length - side_length, side_length / 2 + math.cos(xi) * side_length),
            (height - 2 * math.sin(xi) * side_length - side_length, side_length / 2),
            (aluminium_height / 2, wheel_base_full - wheel_width),
            (-aluminium_height / 2, wheel_base_full - wheel_width)
        ]

        envelope = [
            (-wheel_radius - 0.002, -side_length_env / 2 - math.cos(xi) * side_length_env),
            (height_env - math.sin(xi) * side_length_env, -side_length_env / 2 - math.cos(xi) * side_length_env),
            (height_env, -side_length_env / 2),
            (height_env, side_length_env / 2),
            (height_env - math.sin(xi) * side_length_env, side_length_env / 2 + math.cos(xi) * side_length_env),
            (-wheel_radius - 0.002, side_length_env / 2 + math.cos(xi) * side_length_env)
        ]

        left_wheel_points = [
            (-wheel_radius, -wheel_base_full),
            (wheel_radius, -wheel_base_full),
            (wheel_radius, -(wheel_base_full - wheel_width)),
            (-wheel_radius, -(wheel_base_full - wheel_width))
        ]

        right_wheel_points = [
            {-wheel_radius, wheel_base_full - wheel_width},
            {wheel_radius, wheel_base_full - wheel_width},
            {wheel_radius, wheel_base_full},
            {-wheel_radius, wheel_base_full},
        ]

        Body.__init__(self, state, envelope)
        self.specs: Specs = specs
        self.left_wheel: Wheel = Wheel(specs.wheel_radius, specs.cpr)
        self.right_wheel: Wheel = Wheel(specs.wheel_radius, specs.cpr)

        self.chassis_body = Body(self.state, self.chassis)

        self.left_wheel_body = Body(self.state, left_wheel_points)
        self.right_wheel_body = Body(self.state, right_wheel_points)

        self.wheel_points = [(0, -0.05), (0, 0.05)]

        sensors_states_r = [
            State((self.chassis[3][0] + self.chassis[4][0]) / 2, (self.chassis[3][1] + self.chassis[4][1]) / 2, np.radians(-90)),
            State((self.chassis[4][0] + self.chassis[5][0]) / 2, (self.chassis[4][1] + self.chassis[5][1]) / 2, np.radians(-45)),
            State((self.chassis[5][0] + self.chassis[6][0]) / 2, (self.chassis[5][1] + self.chassis[6][1]) / 2, np.radians(0)),
            State((self.chassis[6][0] + self.chassis[7][0]) / 2, (self.chassis[6][1] + self.chassis[7][1]) / 2, np.radians(45)),
            State((self.chassis[7][0] + self.chassis[8][0]) / 2, (self.chassis[7][1] + self.chassis[8][1]) / 2, np.radians(90)),
        ]

        self.sensors = []

        for sensor_state in sensors_states_r:
            self.sensors.append(Sensor(sensor_state, self))


    def diff_to_uni(self, angular_left, angular_right):
        #pretvorite brzine kotača u translacijsku i kutnu brzinu sustava
        pass

    def uni_to_diff(self, transl_speed, angular_speed):
        #translacijsku i kutnu brzinu sustava pretvorite u kutne brzine kotača
        pass

    def update_state(self, transl_speed, angular_speed, dt):
        if angular_speed == 0:
            self.state.x += transl_speed * math.cos(self.state.phi) * dt
            self.state.y += transl_speed * math.sin(self.state.phi) * dt
        else:
            phi_dt = angular_speed * dt
            self.state.x += 2 * transl_speed / angular_speed * math.cos(self.state.phi + phi_dt / 2) * math.sin(phi_dt / 2)
            self.state.y += 2 * transl_speed / angular_speed * math.sin(self.state.phi + phi_dt / 2) * math.sin(phi_dt / 2)
            self.state.phi += phi_dt

    def move(self, angular_left, angular_right, dt):
        transl_speed, angular_speed = self.diff_to_uni(angular_left, angular_right)
        self.update_state(transl_speed, angular_speed, dt)
        self.left_wheel.update_ticks(angular_left, dt)
        self.right_wheel.update_ticks(angular_right, dt)

    def get_envelope(self):
        return self.envelope

    def update_body(self):
        self.get_envelope_i(True)
        self.chassis_body.get_envelope_i(True, False)
        self.left_wheel_body.get_envelope_i(True, False)
        self.right_wheel_body.get_envelope_i(True, False)
        for sensor in self.sensors:
            sensor.get_envelope_i(True)
            sensor.set_state()
