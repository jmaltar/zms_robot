import matplotlib.pyplot as plt
import math

import numpy

from robots.mathbot import Mathbot
from supervisor.supervisor import Supervisor

plt.ion()


class DynamicPlot:
    #Suppose we know the x range
    """
    min_x = -8
    max_x = 10
    min_y = -14
    max_y = 6
    """
    min_x = -1
    max_x = 3
    min_y = -1
    max_y = 3

    def __init__(self, mathbot: Mathbot, supervisor: Supervisor, color, set_lims=True):
        self.color = color
        self.mathbot = mathbot
        self.supervisor = supervisor
        self.figure, self.ax = plt.subplots(1, 1)
        #Autoscale on unknown axis and known lims on the other
        self.ax.set_autoscaley_on(True)
        if set_lims:
            self.ax.set_xlim(self.min_x, self.max_x)
            self.ax.set_ylim(self.min_y, self.max_y)
        #Other stuff
        self.ax.grid()
        self.ax.set_axisbelow(True)
        self.ax.set_aspect('equal')
        #mng = plt.get_current_fig_manager()
        #mng.window.showMaximized()

        self.envelope = plt.Polygon(mathbot.get_envelope_i(), color="#778899", alpha=0.2)
        self.ax.add_patch(self.envelope)

        goal_x = supervisor.go_to_goal_ctrl.goal.x
        goal_y = supervisor.go_to_goal_ctrl.goal.y
        goal_patch = plt.Circle((goal_x, goal_y), 0.1, color="#C71585", alpha=0.2)
        self.ax.add_patch(goal_patch)

        self.chassis = plt.Polygon(mathbot.chassis_body.get_envelope_i(), color="#20B2AA", alpha=1)
        self.ax.add_patch(self.chassis)

        self.patch1 = plt.Polygon(self.mathbot.left_wheel_body.get_envelope_i(), color="#696969", alpha=1)
        self.patch2 = plt.Polygon(self.mathbot.right_wheel_body.get_envelope_i(), color="#696969", alpha=1)

        self.ax.add_patch(self.patch1)
        self.ax.add_patch(self.patch2)

        self.sensors = []
        for sensor in mathbot.sensors:
            sensor_patch = plt.Polygon(sensor.get_envelope_plot(), color="#4682B4", alpha=0.2)
            self.sensors.append(sensor_patch)
            self.ax.add_patch(sensor_patch)

        self.obstacles = []
        for obstacle in self.supervisor.obstacles:
            obstacle_patch = plt.Polygon(obstacle.get_envelope_i(), color="#696969", alpha=0.5)
            self.obstacles.append(obstacle_patch)
            self.ax.add_patch(obstacle_patch)



    def on_running(self):
        self.envelope.xy = self.mathbot.get_envelope_i()
        self.chassis.xy = self.mathbot.chassis_body.get_envelope_i()
        self.patch1.xy = self.mathbot.left_wheel_body.get_envelope_i()
        self.patch2.xy = self.mathbot.right_wheel_body.get_envelope_i()

        sensor_length = len(self.sensors)

        for i in range(sensor_length):
            self.sensors[i].xy = self.mathbot.sensors[i].get_envelope_plot()
        #Need both of these in order to rescale
        #self.ax.relim()
        #self.ax.autoscale_view()
        #We need to draw *and* flush
        self.figure.canvas.draw()
        self.figure.canvas.flush_events()
