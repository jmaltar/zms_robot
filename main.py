import sys
import math
import time
from threading import Thread

from plot.plot import DynamicPlot


from controllers.go_to_goal import GoToGoal, Goal
from robots.mathbot import Mathbot, State, Specs

from controllers.params import Params
from supervisor.supervisor import Supervisor as GoToGoalSupervisor



# Robot specification
wheel_radius = 0.034
base_length = 0.174
cpr = 930.0
min_rpm = 10
max_rpm = 130
v_max = 2 * math.pi / 60 * max_rpm * wheel_radius

# goal
x_goal = 2
y_goal = 2.5

# params
k_p = 3
k_i = 0.5
k_d = 1

milliseconds = lambda: int(round(time.time() * 1000))
last_time = milliseconds()
current_time = milliseconds()
dt = 0


def watch_time():
    global last_time, current_time, dt
    last_time = current_time
    current_time = milliseconds()
    diff = current_time - last_time
    if diff == 0:
        diff = 1
        time.sleep(0.001)

    dt = diff / 1000


to_stop = False
change = False

mathbot: Mathbot = None
supervisor = None

def simulation():
    global mathbot, supervisor
    mathbot = Mathbot(State(0, 0, math.pi / 2), Specs(wheel_radius, base_length, cpr, v_max, min_rpm, max_rpm))
    go_to_goal_ctrl = GoToGoal(Params(k_p, k_i, k_d), Goal(x_goal, y_goal))
    supervisor = GoToGoalSupervisor(mathbot, go_to_goal_ctrl)

    global last_time, current_time
    last_time = milliseconds()
    current_time = milliseconds()
    global change
    while not supervisor.to_stop:
        watch_time()
        supervisor.execute(dt)
        #supervisor.execute(0.02)
        change = True
        #time.sleep(0.005)
    global to_stop
    to_stop = True


if __name__ == '__main__':

    sim = Thread(target=simulation)
    sim.start()
    if mathbot is None:
        while True:
            if mathbot is not None:
                break
    if supervisor is None:
        while True:
            if supervisor is not None:
                break

    d = DynamicPlot(mathbot, supervisor, "#FF8C00")
    while not to_stop:
        d.on_running()
        #time.sleep(1/60)
    print('End')
    time.sleep(1)
    sys.exit()